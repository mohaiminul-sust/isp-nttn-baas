{% load i18n %}{% blocktrans with site_name='Vertical Innovations' %}
Hi,

Thank you for connecting your social account with {{ site_name }}. You can now login with your connected social account at {{ site_name }} with ease.
{% endblocktrans %}
Follow the link below to go to dashboard to complete signup process.
{% block redirect_link %}
https://shop.vertical-innovations.com/users/dashboard
{% endblock %}
{% blocktrans with site_name='Vertical Innovations' %}
With regards from,
{{ site_name }}{% endblocktrans %}