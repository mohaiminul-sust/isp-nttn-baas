# ISP NTTN Pop Management BaaS (API + Admin Dashboard)

## Features
* Fully documented API with CoreAPI and Postman
* Custom Auth System with password reset, update APIs and customized user accounts and $
* Fully customized entity mangement on each module with tabular and stacked inlines
* Leaflet GeoAdmin integration for location based entity management
* Fullmap custom page with entity and location (distance, lat, lon) based optimized fil$
* Tablib based import/export mixins for all/selective entity export from admin
* 2D Geospatial location data storage with postgis extension for postgresql
* Location based ISP/NTTN/Pop query APIs (Sorted for nearest)
* Sentry error tracker integration with user tracking


## Tech Stack
* Django - 3.0.5
* Django REST Framework - 3.11.0
* Certifi - 2020.6.20
* REST Auth - 0.9.5
* AnyMail (Mailgun : Mail Service Provider) - 7.1.0
* Grappelli Admin Interface - 2.14.2
* CoreAPI Docs - 2.3.3
* Gunicorn - 20.0.4
* Sentry - 0.14.4
* PostGIS connector for geospatial data
* Tablib (Entity import / export module) - 2.0.0 
* Django Leaflet - 0.27.1
 
check full requirements list [here](requirements.txt)...

## Getting Started
Setup project environment with [virtualenvwrapper](https://virtualenvwrapper.readthedocs.io/en/latest/install.html) and [pip](https://pip.pypa.io).

```bash
$ mkvirtualenv ispapi
$ workon ispapi

# clone the repo, navigate to the project and then ...
$ pip install -r requirements.txt

# Configure Postgresql DB according to project settings and run the command below
$ ./manage.py migrate

# Just run the server
$ ./manage.py runserver 0.0.0.0:8000

# Run tests
$ ./manage.py test
```

For API documentation visit [here](https://documenter.getpostman.com/view/198290/T17GenBg?version=latest)
