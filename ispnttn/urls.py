"""ispnttn URL Configuration

"""

from django.conf.urls import url
from django.conf import settings
from django.conf.urls import handler404
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, reverse_lazy
from django.views.generic.base import RedirectView
from django.views.generic import TemplateView

from rest_framework.documentation import include_docs_urls

# admin.site.site_header = "ISP NTTN"
# admin.site.site_title = "ISP NTTN"
admin.site.index_title = "Dashboard"
admin.site.site_url = settings.UI_BASE
doc_title = 'ISP NTTN API Documentation'
handler404 = 'users.views.error_404_view'

urlpatterns = [
    url(r'^$', RedirectView.as_view(url=reverse_lazy('admin:index'))),
    path('grappelli/', include('grappelli.urls')), # grappelli URLS
    url(r'^admin/', admin.site.urls),
    path(r'', include('users.urls')),
    path(r'', include('isp_management.urls')),
    path(r'api-docs/', include_docs_urls(title=doc_title))
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += [
    path('css/base_styles.css', TemplateView.as_view(
        template_name='css/base_styles.css',
        content_type='text/css')
    ),
    path('css/map_styles.css', TemplateView.as_view(
        template_name='css/map_styles.css',
        content_type='text/css')
    ),
]