from django.contrib.auth import authenticate
from django.shortcuts import get_object_or_404
from django.utils.http import urlencode
from django.core import serializers
from django.utils.crypto import get_random_string
from django.core.mail import send_mail
from django.template import Context, loader
from django.conf import settings

from rest_framework import generics, status, viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view

from rest_auth.registration.views import LoginView, RegisterView, VerifyEmailView

from .models import User, ForgetPassword
from .serializers import UserSerializer, AccountRegisterSerializer
from .convenience import get_parts_from_name


#API Views
class RestRegisterView(RegisterView):
    authentication_classes = (TokenAuthentication,)
    serializer_class = AccountRegisterSerializer


class RestLoginView(LoginView):
    authentication_classes = (TokenAuthentication,)


class RestVerifyEmailView(VerifyEmailView):
    authentication_classes = (TokenAuthentication,)


@api_view()
def null_view(request):
    return Response(status=status.HTTP_400_BAD_REQUEST)

@api_view()
def complete_view(request):
    return Response("Email account is activated")


class ForgetPasswordAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def post(self, request):
        if 'email' not in request.data:
            raise NotAcceptable('Must include email in request')
        email = request.data['email'].strip()

        user = User.objects.filter(email=email)

        if user.exists():
            user= user.first()

            new_pass = get_random_string(length=16)
            user.set_password(new_pass)
            user.save()

            ForgetPassword.objects.create(user=user, new_password=new_pass)

            my_template = loader.get_template('account/email/password_forgot_message.txt')
            my_context = Context({
                'temp_pass': new_pass,
            })
            forgot_pass_message = my_template.render(my_context.flatten())
            send_mail("Forgot your password!", forgot_pass_message, settings.DEFAULT_FROM_EMAIL, [email])

            return Response({
                'detail': 'Mail Sent!',
            })
        else:
            raise NotFound('You\'re not registered!')