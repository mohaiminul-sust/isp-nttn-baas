from allauth.account.signals import user_signed_up, password_changed, email_confirmed
# from allauth.socialaccount.signals import social_account_added
from django.contrib.auth.signals import user_logged_in
from django.core.mail import send_mail
from django.template import Context, loader
from django.conf import settings
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save
from datetime import datetime
from django.utils.timezone import now

from .models import User
from isp_management.models import Provider


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        Provider.objects.create(user=instance)


# login signal
@receiver(user_logged_in)
def user_logged_in_handler(sender, user, request, **kwargs):
    user.last_login = now()
    user.save()


# allauth signals
# @receiver(user_signed_up)
# def user_signed_up(request, user, **kwargs):
#     if user.can_receive_mail():
#         my_template = loader.get_template('account/email/after_signup_message.txt')
#         my_context = Context({
#             'user': user,
#         })
#         after_signup_email_message = my_template.render(my_context.flatten())
#         send_mail("Welcome to VI!", after_signup_email_message,
#             settings.DEFAULT_FROM_EMAIL, [user.email])


# @receiver(password_changed)
# def password_changed(request, user, **kwargs):
#     if user.can_receive_sms():
#         send_sms(user.sms_number(), 'Your password has been successfully updated.')

#     if user.can_receive_mail():
#         my_template = loader.get_template('account/email/password_updated_message.txt')
#         my_context = Context({
#             'user': user,
#         })
#         pass_update_email_message = my_template.render(my_context.flatten())
#         send_mail("Password changed successfully!", pass_update_email_message,
#             settings.DEFAULT_FROM_EMAIL, [user.email])


# @receiver(email_confirmed)
# def email_confirmed(request, email_address, **kwargs):
#     my_template = loader.get_template('account/email/after_email_confirmed_message.txt')
#     my_context = Context({
#         'email': email_address,
#     })
#     after_email_verify_message = my_template.render(my_context.flatten())
#     send_mail("Email confirmed and verified!", after_email_verify_message,
#           settings.DEFAULT_FROM_EMAIL, [email_address])


# @receiver(social_account_added)
# def social_account_added(request, sociallogin, **kwargs):
#     user = sociallogin.account.user
#     my_template = loader.get_template('account/email/social_account_connected_message.txt')
#     my_context = Context({
#         'user': user,
#     })
#     social_account_connected_message = my_template.render(my_context.flatten())
#     send_mail("Social Account connected with VI!", social_account_connected_message,
#           settings.DEFAULT_FROM_EMAIL, [user.email])