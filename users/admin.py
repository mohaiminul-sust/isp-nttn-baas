from django.contrib import admin
from django.conf import settings
from django.contrib.auth.admin import UserAdmin
# from django.contrib.auth.models import Permission

from .models import User, ForgetPassword
from isp_management.models import Provider
# for debugging permission objects (e.g. deleting leftovers from unused/deleted models)
# admin.site.register(Permission)

class ProviderInline(admin.StackedInline):
    model = Provider
    can_delete = False
    show_change_link = True

class UserAdmin(UserAdmin):
    date_hierarchy = 'date_joined'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    inlines = (ProviderInline,)
    list_display = ('email', 'id', 'account_type', 'staff_status', 'last_login', 'date_joined')
    list_filter = ('groups', 'is_staff')
    search_fields = ('email',)

    def account_type(self, obj):
        return obj.assigned_group()

    def staff_status(self, obj):
        return obj.is_staff
    staff_status.boolean = True

    def get_inlines(self, request, obj=None):
        if obj:
            return self.inlines
        else:
            return []

admin.site.register(User, UserAdmin)


class ForgetPasswordAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('user', 'created_date')
    ordering = ('-created_date',)
    search_fields = ('email',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return True

admin.site.register(ForgetPassword, ForgetPasswordAdmin)