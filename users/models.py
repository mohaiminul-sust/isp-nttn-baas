from django.db import models
# from django.db.models import Sum
from uuid import uuid4
from django.contrib.auth.models import AbstractUser

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.conf import settings

from .usermanager import UserManager


class User(AbstractUser):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)
    email = models.EmailField(max_length=254, unique=True)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []
    objects = UserManager()

    def __str__(self):
        return self.email if self.email.strip() != '' else str(self.id)

    def assigned_group(self):
        return self.groups.values_list('name', flat = True)[0] if self.groups.exists() else '-'


class ForgetPassword(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, related_name='product_limit_user', help_text="Select User")
    new_password = models.CharField(_("Temporary Password"), max_length=254, default='')
    created_date = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Forget Password Requests"

    def __str__(self):
        return self.id
