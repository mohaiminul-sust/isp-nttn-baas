from django.conf import settings
from django.contrib.sites.models import Site
import os


def get_formatted_name(text):
    l_cased = text.lower()
    name_str = l_cased[0].upper() + l_cased[1:len(l_cased)]
    return name_str


def get_parts_from_name(full_name):
    if full_name == '':
        return ('', '')
    name_arr = full_name.split(" ", 1)
    first = get_formatted_name(name_arr[0])
    last = get_formatted_name(name_arr[1]) if len(name_arr) > 1 else ""
    return (first, last)


def get_current_domain():
    return Site.objects.get_current().domain


def asset_url_for(path):
    if not str(path) == "":
        return get_current_domain() + os.path.join(settings.MEDIA_URL, str(path))
    else:
        return None