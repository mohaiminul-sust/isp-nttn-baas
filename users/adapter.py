from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
# from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from .models import User
from allauth.account.models import EmailAddress


class LoginAccountAdapter(DefaultAccountAdapter):
    def get_login_redirect_url(self, request):
        # redirection path for user after social login redirect
        path = "/users/{userid}/token/"
        return path.format(userid=request.user.id)


# class SocialAccountAdapter(DefaultSocialAccountAdapter):
#     def pre_social_login(self, request, sociallogin):
#         # Ignore existing social accounts, just do this stuff for new ones
#         if sociallogin.is_existing:
#             return

#         if 'email' not in sociallogin.account.extra_data:
#             return

#         # check if given email address already exists.
#         # Note: __iexact is used to ignore cases
#         try:
#             email = sociallogin.account.extra_data['email'].lower()
#             # eusers = User.objects.filter(email=email)
#             # if eusers:
#             #     perform_login(request, eusers[0], email_verification=app_settings.EMAIL_VERIFICATION)
#             #     # sociallogin.connect(request, user)
#             #     # return

#             email_address = EmailAddress.objects.get(email__iexact=email)
#         # if it does not, let allauth take care of this new social account
#         except EmailAddress.DoesNotExist:
#             return

#         user = email_address.user
#         sociallogin.connect(request, user)