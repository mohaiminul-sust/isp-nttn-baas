from django.utils.translation import gettext_lazy as _
from django.urls import reverse

from grappelli.dashboard import modules, Dashboard
from grappelli.dashboard.utils import get_admin_site_name


class CustomIndexDashboard(Dashboard):
    """
    Custom index dashboard for isp admin
    """
    def init_with_context(self, context):
        site_name = get_admin_site_name(context)

        self.children.append(modules.AppList(
            _('Manage Providers'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('isp_management.models.Provider', 'isp_management.models.ProviderPop','isp_management.models.IspType',),
        ))

        self.children.append(modules.AppList(
            _('Manage Locations'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('isp_management.models.Division', 'isp_management.models.District','isp_management.models.Upazilla', 'isp_management.models.Ward',),
        ))

        self.children.append(modules.AppList(
            _('Platform Management'),
            collapsible=True,
            column=1,
            css_classes=('collapse closed',),
            models=('isp_management.models.PlatformConfig',),
        ))

        self.children.append(modules.LinkList(
            _('Full Maps'),
            column=2,
            children=[
                {
                    'title': _('Show Pop Map'),
                    'url': '/fullmap/',
                    'external': False,
                },
            ]
        ))

        self.children.append(modules.ModelList(
            _('Manage Accounts'),
            collapsible=True,
            column=2,
            models=('users.models.User', 'django.contrib.auth.models.Group', 'users.models.ForgetPassword', 'rest_framework.authtoken.models.Token'),
        ))

        # append an app list module for "Administration"
        self.children.append(modules.ModelList(
            _('Developer Only: Administration'),
            column=2,
            collapsible=False,
            models=('django.contrib.sites.models.Site',),
        ))

        # append another link list module for "support".
        # self.children.append(modules.LinkList(
        #     _('Support'),
        #     column=2,
        #     children=[
        #         {
        #             'title': _('Django Documentation'),
        #             'url': 'http://docs.djangoproject.com/',
        #             'external': True,
        #         },
        #         {
        #             'title': _('Grappelli Documentation'),
        #             'url': 'http://packages.python.org/django-grappelli/',
        #             'external': True,
        #         },
        #         {
        #             'title': _('Grappelli Google-Code'),
        #             'url': 'http://code.google.com/p/django-grappelli/',
        #             'external': True,
        #         },
        #     ]
        # ))

        # append a feed module
        # self.children.append(modules.Feed(
        #     _('Latest Django News'),
        #     column=2,
        #     feed_url='http://www.djangoproject.com/rss/weblog/',
        #     limit=5
        # ))

        # append a recent actions module
        self.children.append(modules.RecentActions(
            _('Recent actions'),
            limit=10,
            collapsible=True,
            column=3,
        ))
