from django.utils.translation import gettext as _


PROVIDER_TYPES = (
    (1, _("ISP")),
    (2, _("NTTN")),
)

POP_TYPES = (
    (1, _("OWN")),
    (2, _("SHARED")),
)