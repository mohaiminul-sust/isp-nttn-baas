# Generated by Django 3.0.7 on 2020-07-04 17:23

import django.contrib.gis.db.models.fields
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('isp_management', '0007_auto_20200701_1737'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='provider',
            options={'verbose_name_plural': 'Providers (ISP/NTTN)'},
        ),
        migrations.AddField(
            model_name='provider',
            name='address',
            field=models.TextField(blank=True, verbose_name='Address'),
        ),
        migrations.AddField(
            model_name='provider',
            name='district',
            field=models.ForeignKey(blank=True, help_text='Select District', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='providers', to='isp_management.District'),
        ),
        migrations.AddField(
            model_name='provider',
            name='division',
            field=models.ForeignKey(blank=True, help_text='Select Division', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='providers', to='isp_management.Division'),
        ),
        migrations.AddField(
            model_name='provider',
            name='lat',
            field=models.FloatField(blank=True, null=True, verbose_name='Latitude'),
        ),
        migrations.AddField(
            model_name='provider',
            name='loc',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, editable=False, geography=True, null=True, srid=4326, verbose_name='Location'),
        ),
        migrations.AddField(
            model_name='provider',
            name='lon',
            field=models.FloatField(blank=True, null=True, verbose_name='Longitude'),
        ),
        migrations.AddField(
            model_name='provider',
            name='provider_type',
            field=models.IntegerField(choices=[(1, 'ISP'), (2, 'NTTN')], default=1, verbose_name='Provider Type'),
        ),
        migrations.AlterField(
            model_name='provider',
            name='isp_type',
            field=models.ForeignKey(blank=True, help_text='Select ISP Type (Only If ISP)', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='type', to='isp_management.IspType'),
        ),
        migrations.AlterField(
            model_name='provider',
            name='name',
            field=models.CharField(default='', max_length=150, verbose_name='Name of Provider'),
        ),
        migrations.AlterField(
            model_name='providerpop',
            name='isp_name',
            field=models.ForeignKey(blank=True, help_text='Select Provider', null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pops', to='isp_management.Provider'),
        ),
        migrations.AlterField(
            model_name='providerpop',
            name='lat',
            field=models.FloatField(blank=True, null=True, verbose_name='Latitude'),
        ),
        migrations.AlterField(
            model_name='providerpop',
            name='lon',
            field=models.FloatField(blank=True, null=True, verbose_name='Longitude'),
        ),
        migrations.DeleteModel(
            name='NTTN',
        ),
    ]
