# Generated by Django 3.0.7 on 2020-08-13 15:49

import django.contrib.gis.db.models.fields
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('isp_management', '0016_auto_20200809_1815'),
    ]

    operations = [
        migrations.AlterField(
            model_name='providerpop',
            name='loc',
            field=django.contrib.gis.db.models.fields.PointField(blank=True, geography=True, null=True, srid=4326, verbose_name='Location'),
        ),
    ]
