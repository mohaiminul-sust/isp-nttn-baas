# Generated by Django 3.0.7 on 2020-07-25 00:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('isp_management', '0011_auto_20200707_0327'),
    ]

    operations = [
        migrations.AlterField(
            model_name='ward',
            name='name',
            field=models.CharField(help_text='Name or Number', max_length=150, verbose_name='Name'),
        ),
    ]
