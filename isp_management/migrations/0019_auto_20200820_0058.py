# Generated by Django 3.0.7 on 2020-08-19 18:58

import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('isp_management', '0018_auto_20200813_2207'),
    ]

    operations = [
        migrations.AddField(
            model_name='provider',
            name='contact_name',
            field=models.CharField(blank=True, max_length=150, verbose_name='Contact Name'),
        ),
        migrations.AddField(
            model_name='provider',
            name='contact_number',
            field=models.CharField(blank=True, max_length=14, validators=[django.core.validators.RegexValidator(message="Contact number must be entered in the format: '+8801234567890'", regex='^\\+88\\d{11}$')], verbose_name='Contact Number'),
        ),
    ]
