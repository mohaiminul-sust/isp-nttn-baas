from django.apps import AppConfig


class IspManagementConfig(AppConfig):
    name = 'isp_management'
    verbose_name = ''

    def ready(self):
        import isp_management.signals