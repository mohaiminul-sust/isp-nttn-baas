from django.forms import ModelForm
from .models import ProviderPop


class ProviderPopForm(ModelForm):
    class Meta:
        model = ProviderPop
        fields = '__all__'

    def clean(self):
        pop_type = self.cleaned_data.get('pop_type')
        operator_name = self.cleaned_data.get('operator_name')

        if pop_type == 2:
            if operator_name.strip() == "":
                msg = "Operator name is required for shared POP type!"
                self._errors['operator_name'] = self.error_class([msg])
                del self.cleaned_data['operator_name']