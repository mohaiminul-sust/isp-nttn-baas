from django.db import models
from django.contrib.gis.db import models

from datetime import date, datetime
from dateutil.relativedelta import relativedelta
from django.utils.timezone import now
from django.utils.translation import gettext as _
from django.conf import settings
from django.core.validators import RegexValidator

from users.models import User
from .choices import PROVIDER_TYPES, POP_TYPES


class PlatformConfig(models.Model):
    config_name = models.CharField(_("Name"), max_length=150, blank=False, null=False, help_text="Config Name")
    map_filter_radius = models.PositiveIntegerField(_("Map's Filter Radius (In Km)"), default=5, blank=False, null=False, help_text="In kilometers (e.g. 5 means 5km)")
    active = models.BooleanField(_("Is Active ?"), default=False, help_text="Activate Config")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Platform Configurations"

    def __str__(self):
        return self.config_name


class Division(models.Model):
    name = models.CharField(_("Name"), max_length=150, blank=False, null=False, help_text="Name")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Divisions"

    def __str__(self):
        return self.name

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)


class District(models.Model):
    name = models.CharField(_("Name"), max_length=150, blank=False, null=False, help_text="Name")
    division = models.ForeignKey(Division, on_delete=models.CASCADE, null=True, blank=True, related_name='districts', help_text="Select Division")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Districts"

    def __str__(self):
        return self.name

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)


class Upazilla(models.Model):
    name = models.CharField(_("Name"), max_length=150, blank=False, null=False, help_text="Name")
    district = models.ForeignKey(District, on_delete=models.CASCADE, null=True, blank=True, related_name='upazillas', help_text="Select District")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Upazillas"

    def __str__(self):
        return self.name

    def division(self):
        return self.district.division if self.district is not None else None

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)


class Ward(models.Model):
    name = models.CharField(_("Name"), max_length=150, blank=False, null=False, help_text="Name or Number")
    upazilla = models.ForeignKey(Upazilla, on_delete=models.CASCADE, null=True, blank=True, related_name='wards', help_text="Select Upazilla")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Wards"

    def __str__(self):
        return self.name

    def district(self):
        return self.upazilla.district if self.upazilla is not None else None

    def division(self):
        return self.upazilla.division() if self.upazilla is not None else None

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)



class IspType(models.Model):
    name = models.CharField(_("Name"), max_length=150, blank=False, null=False, help_text="Name")
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "ISP Types"

    def __str__(self):
        return self.name

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)


class Provider(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='isp', on_delete=models.CASCADE)
    provider_type = models.IntegerField(_("Provider Type"), choices=PROVIDER_TYPES, default=1)
    name = models.CharField(_("Name of Provider"), max_length=150, blank=False, null=True, default="", help_text="ISP name / NTTN identifier or name")
    isp_type = models.ForeignKey(IspType, on_delete=models.SET_NULL, null=True, blank=True, related_name='type', help_text="Select ISP Type (Only If ISP)")
    address = models.TextField(_("Address"), blank=True, null=True)
    contact_name = models.CharField(_("Contact Name"), max_length=150, blank=True, null=True)
    contact_regex = RegexValidator(regex=settings.PHONE_VALIDATION_REGEX, message="Contact number must be entered in the format: '+8801234567890'")
    contact_number = models.CharField(_("Contact Number"), validators=[contact_regex],max_length=14, blank=True, null=True)
    division = models.ForeignKey(Division, on_delete=models.SET_NULL, null=True, blank=True, related_name='providers', help_text="Select Division")
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True, blank=True, related_name='providers', help_text="Select District")
    lat = models.FloatField(_("Latitude"), blank=True, null=True, help_text="Optional")
    lon = models.FloatField(_("Longitude"), blank=True, null=True, help_text="Optional")
    loc = models.PointField(_("Location"), srid=4326, geography=True, dim=2, null=True, blank=True, editable=True)
    active = models.BooleanField(_("Active"), default=True)
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "Providers (ISP/NTTN)"

    def __str__(self):
        return self.name

    @staticmethod
    def autocomplete_search_fields():
        return ("name__icontains",)

    def total_pops(self):
        return ProviderPop.objects.filter(provider=self).count()


class ProviderPop(models.Model):
    provider = models.ForeignKey(Provider, on_delete=models.CASCADE, null=True, blank=True, related_name='pops', help_text="Select Provider")
    pop_address = models.TextField(_("POP Address"), blank=True)
    pop_type =  models.IntegerField(_("POP Type"), choices=POP_TYPES, default=1)
    operator_name = models.CharField(_("Operator Name"), max_length=150, blank=True)
    division = models.ForeignKey(Division, on_delete=models.SET_NULL, null=True, blank=True, related_name='pops', help_text="Select Division")
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True, blank=True, related_name='pops', help_text="Select District")
    upazilla = models.ForeignKey(Upazilla, on_delete=models.SET_NULL, null=True, blank=True, related_name='pops', help_text="Select Upazilla")
    ward = models.ForeignKey(Ward, on_delete=models.SET_NULL, null=True, blank=True, related_name='pops', help_text="Select Ward")
    lat = models.FloatField(_("Latitude"), blank=True, null=True)
    lon = models.FloatField(_("Longitude"), blank=True, null=True)
    loc = models.PointField(_("Location"), srid=4326, geography=True, dim=2, null=True, blank=True, editable=True)
    contact_info = models.TextField(_("Contact Information"), blank=True)
    equipments = models.TextField(_("Equipments"), blank=True)
    active = models.BooleanField(_("Active"), default=True)
    created_date = models.DateField(auto_now=False, auto_now_add=True)
    updated_date = models.DateField(auto_now=True, auto_now_add=False)

    class Meta:
        verbose_name_plural = "All POPs"

    def __str__(self):
        return str(self.id)