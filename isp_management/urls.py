from django.urls import path, include
from django.conf.urls import url

from .apiviews import ProviderListAPIView, ProviderViewSet, IspTypeListAPIView, DivisionListAPIView, DistrictListAPIView, UpazillaListAPIView, WardListAPIView, ProviderPopListAPIView, ProviderPopCreateAPIView, ProviderPopRetrieveUpdateDestroyAPIView
from .views import PopMapView

urlpatterns = [
    path('providers/me/', ProviderViewSet.as_view(), name='provider_view_set'),
    path('providers/', ProviderListAPIView.as_view(), name='provider_list_view'),

    path('isp-types/', IspTypeListAPIView.as_view(), name='isp_types_list_view'),
    path('divisions/', DivisionListAPIView.as_view(), name='division_list_view'),
    path('districts/', DistrictListAPIView.as_view(), name='district_list_view'),
    path('upazillas/', UpazillaListAPIView.as_view(), name='upazilla_list_view'),
    path('wards/', WardListAPIView.as_view(), name='wards_list_view'),

    path('pops/create/', ProviderPopCreateAPIView.as_view(), name='pop_create_view'),
    path('pops/<int:id>/', ProviderPopRetrieveUpdateDestroyAPIView.as_view(), name='pop_rud_viewset'),
    path('pops/', ProviderPopListAPIView.as_view(), name='pop_list_view'),
    path('fullmap/', PopMapView, name='pop_fullmap'),
]