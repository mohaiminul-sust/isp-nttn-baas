from django.shortcuts import render
from django.template import loader
from django.http import HttpResponse, HttpResponseNotFound
from django.core.serializers import serialize
from django.utils.html import format_html
from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.contrib.gis.db.models.functions import GeometryDistance

from .serializers import ProviderPopMapViewSerializer
from .models import ProviderPop, Provider, IspType, Division, District, Upazilla, Ward
from .convenience import get_config

import json, re


# Create your views here.
def PopMapView(request):
    template = loader.get_template('map/pop_view.html')
    isp_types = IspType.objects.all().order_by('name')
    providers = Provider.objects.all().order_by('name')
    divisions = Division.objects.all().order_by('name')
    districts = District.objects.all().order_by('name')
    upazillas = Upazilla.objects.all().order_by('name')
    wards = Ward.objects.all().order_by('name')

    all_pops = ProviderPop.objects.filter(active=True)
    if request.GET.get('isp-type'):
        isp_type_id = request.GET.get('isp-type')
        all_pops = all_pops.filter(provider__isp_type__id=isp_type_id)
    elif request.GET.get('provider'):
        provider_id = request.GET.get('provider')
        all_pops = all_pops.filter(provider__user__id=provider_id)
    elif request.GET.get('division'):
        division_id = request.GET.get('division')
        all_pops = all_pops.filter(division__id=division_id)
    elif request.GET.get('district'):
        district_id = request.GET.get('district')
        all_pops = all_pops.filter(district__id=district_id)
    elif request.GET.get('upazilla'):
        upazilla_id = request.GET.get('upazilla')
        all_pops = all_pops.filter(upazilla__id=upazilla_id)
    elif request.GET.get('ward'):
        ward_id = request.GET.get('ward')
        all_pops = all_pops.filter(ward__id=ward_id)
    elif request.GET.get('all'):
        is_all = request.GET.get('all')
        all_pops = all_pops[:None if is_all else 300]
    elif request.GET.get('lat') and request.GET.get('lon'):
        try:
            user_lat = float(request.GET.get('lat'))
            user_lon = float(request.GET.get('lon'))
            user_loc = Point(user_lon, user_lat, srid=4326)

            radius_km = get_config().map_filter_radius if get_config() is not None else 5
            if request.GET.get('rad'):
                try:
                    radius_km = float(request.GET.get('rad'))
                    if radius_km < 0:
                        radius_km = radius_km * (-1)
                except:
                    pass
            radius = radius_km * 1000

            all_pops = all_pops.filter(
                loc__distance_lt=(
                    user_loc,
                    Distance(m=radius)
                )
            ).order_by(GeometryDistance("loc", user_loc))
        except:
            pass
    else:
        all_pops = all_pops[:300]

    all_pops = ProviderPopMapViewSerializer.with_eager_load(all_pops)
    all_pops_data = ProviderPopMapViewSerializer(all_pops, many=True).data

    context = {
        "pops_count": len(all_pops_data),
        "pops": json.dumps(all_pops_data ,ensure_ascii=False).encode('utf8').decode(),
        "isp_types": isp_types,
        "providers": providers,
        "divisions": divisions,
        "districts": districts,
        "upazillas": upazillas,
        "wards": wards,
    }
    return HttpResponse(template.render(context, request))