from rest_framework import serializers
from django.conf import settings
from django.utils.html import escape

from .models import IspType, Division, District, Upazilla, Ward, Provider, ProviderPop


class IspTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = IspType
        fields = ('id', 'name',)


class DivisionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Division
        fields = ('id', 'name',)


class DistrictSerializer(serializers.ModelSerializer):
    # division = serializers.CharField()
    class Meta:
        model = District
        fields = ('id', 'name',)

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('division')
        return queryset


class UpazillaSerializer(serializers.ModelSerializer):
    # division = serializers.CharField()
    # district = serializers.CharField()
    class Meta:
        model = Upazilla
        fields = ('id', 'name',)

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('district')
        return queryset


class WardSerializer(serializers.ModelSerializer):
    # division = serializers.CharField()
    # district = serializers.CharField()
    # upazilla = serializers.CharField()
    class Meta:
        model = Ward
        fields = ('id', 'name',)

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('upazilla')
        return queryset


class ProviderSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    class Meta:
        model = Provider
        fields = ('id', 'name', 'provider_type', 'isp_type', 'address', 'contact_name', 'contact_number', 'lat', 'lon', 'division', 'district',)

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('isp_type')
        queryset = queryset.select_related('division')
        queryset = queryset.select_related('district')
        return queryset

    def get_id(self, obj):
        return str(obj.user.id)


class ProviderListSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    provider_type = serializers.SerializerMethodField()
    isp_type = IspTypeSerializer(read_only=True)
    division = DivisionSerializer(read_only=True)
    district = DistrictSerializer(read_only=True)
    total_pops = serializers.SerializerMethodField()
    class Meta:
        model = Provider
        fields = ('id', 'name', 'total_pops','provider_type', 'isp_type', 'address', 'contact_name', 'contact_number', 'lat', 'lon', 'division', 'district',)

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('isp_type')
        queryset = queryset.select_related('division')
        queryset = queryset.select_related('district')
        return queryset

    def get_id(self, obj):
        return str(obj.user.id)

    def get_provider_type(self, obj):
        return obj.get_provider_type_display()

    def get_total_pops(self, obj):
        return obj.total_pops()


class ProviderDropdownListSerializer(serializers.ModelSerializer):
    id = serializers.SerializerMethodField()
    class Meta:
        model = Provider
        fields = ('id', 'name',)

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('user')
        return queryset

    def get_id(self, obj):
        return str(obj.user.id)


class ProviderPopSerializer(serializers.ModelSerializer):
    provider_name = serializers.CharField(source="provider.name", read_only=True)
    provider_type = IspTypeSerializer(source="provider.isp_type", read_only=True)
    class Meta:
        model = ProviderPop
        fields = ('id', 'provider_name', 'provider_type', 'pop_address', 'lat', 'lon', 'division', 'district', 'upazilla', 'ward', 'contact_info', 'equipments')

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('provider')
        queryset = queryset.select_related('division')
        queryset = queryset.select_related('district')
        queryset = queryset.select_related('upazilla')
        queryset = queryset.select_related('ward')
        return queryset


class ProviderPopListSerializer(serializers.ModelSerializer):
    provider = ProviderDropdownListSerializer(read_only=True)
    isp_type = IspTypeSerializer(source="provider.isp_type", read_only=True)
    division = DivisionSerializer(read_only=True)
    district = DistrictSerializer(read_only=True)
    upazilla = UpazillaSerializer(read_only=True)
    ward = WardSerializer(read_only=True)
    class Meta:
        model = ProviderPop
        fields = ('id', 'provider', 'isp_type', 'pop_address', 'lat', 'lon', 'loc', 'division', 'district', 'upazilla', 'ward', 'contact_info', 'equipments')

    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('provider')
        queryset = queryset.select_related('division')
        queryset = queryset.select_related('district')
        queryset = queryset.select_related('upazilla')
        queryset = queryset.select_related('ward')
        return queryset


class ProviderPopMapViewSerializer(serializers.ModelSerializer):
    provider = serializers.SerializerMethodField()
    isp_type = serializers.SerializerMethodField()
    # address = serializers.SerializerMethodField()
    class Meta:
        model = ProviderPop
        fields = ('id', 'lat', 'lon', 'provider', 'isp_type', 'pop_address', 'contact_info')


    @staticmethod
    def with_eager_load(queryset):
        queryset = queryset.select_related('provider')
        return queryset

    def get_provider(self, obj):
        return obj.provider.name

    def get_isp_type(self, obj):
        return obj.provider.isp_type.name

    # def get_address(self, obj):
    #     return escape(obj.pop_address).encode("ascii", "xmlcharrefreplace").decode()