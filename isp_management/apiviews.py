from django.core import serializers

from rest_framework import generics, status, viewsets, mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.exceptions import PermissionDenied, NotFound, NotAcceptable
from rest_framework.authentication import TokenAuthentication
from rest_framework.decorators import api_view

from django.contrib.gis.geos import Point
from django.contrib.gis.measure import Distance
from django.contrib.gis.db.models.functions import GeometryDistance
from django.db.models import Q
import operator
from functools import reduce

from .models import IspType, Division, District, Upazilla, Ward, Provider, ProviderPop
from .serializers import IspTypeSerializer, DivisionSerializer, DistrictSerializer, UpazillaSerializer, WardSerializer, ProviderSerializer, ProviderListSerializer, ProviderPopSerializer, ProviderPopListSerializer, ProviderDropdownListSerializer
from .convenience import get_config


class IspTypeListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        isp_types = IspType.objects.all()
        types_data = IspTypeSerializer(isp_types, many=True).data

        return Response({
            "data": types_data
        })


class DivisionListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        divisions = Division.objects.all().order_by("name")
        divisions_data = DivisionSerializer(divisions, many=True).data

        return Response({
            "data": divisions_data
        })


class DistrictListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        districts = District.objects.all()

        if 'division' in request.query_params:
            div_id = request.query_params['division']
            districts = districts.filter(division__id=div_id)

        districts = districts.order_by("name")
        districts = DistrictSerializer.with_eager_load(districts)
        districts_data = DistrictSerializer(districts, many=True).data

        return Response({
            "data": districts_data
        })


class UpazillaListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        upazillas = Upazilla.objects.all()

        f_list = []
        if 'district' in request.query_params:
            dist_id = request.query_params['district']
            f_list.append(Q(district__id=dist_id))

        if 'division' in request.query_params:
            div_id = request.query_params['division']
            f_list.append(Q(district__division__id=div_id))

        if len(f_list) > 0:
            upazillas = upazillas.filter(reduce(operator.or_, f_list))

        upazillas = upazillas.order_by("name")
        upazillas = UpazillaSerializer.with_eager_load(upazillas)
        upazillas_data = UpazillaSerializer(upazillas, many=True).data

        return Response({
            "data": upazillas_data
        })


class WardListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        wards = Ward.objects.all()

        if 'upazilla' in request.query_params:
            upz_id = request.query_params['upazilla']
            wards = wards.filter(upazilla__id=upz_id)

        if 'district' in request.query_params:
            dist_id = request.query_params['district']
            wards = wards.filter(upazilla__district__id=dist_id)

        if 'division' in request.query_params:
            div_id = request.query_params['division']
            wards = wards.filter(upazilla__district__division__id=div_id)

        wards = wards.order_by("name")
        wards = WardSerializer.with_eager_load(wards)
        wards_data = WardSerializer(wards, many=True).data

        return Response({
            "data": wards_data
        })


class ProviderListAPIView(APIView):
    authentication_classes = []
    permission_classes = []
    def get(self, request):
        f_list = []
        if 'provider_type' in request.query_params:
            provider_type = request.query_params['provider_type']
            f_list.append(Q(provider_type=provider_type))

        if 'isp_type' in request.query_params:
            type_id = request.query_params['isp_type']
            f_list.append(Q(isp_type__id=type_id))

        providers = Provider.objects.filter(active=True)
        if len(f_list) > 0:
            providers = providers.filter(reduce(operator.or_, f_list))

        providers = ProviderDropdownListSerializer.with_eager_load(providers)
        providers_data = ProviderDropdownListSerializer(providers, many=True).data

        return Response({
            "data": providers_data
        })


class ProviderViewSet(generics.RetrieveUpdateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = Provider.objects.all()

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ProviderListSerializer
        return ProviderSerializer

    def get_object(self):
        queryset = self.filter_queryset(self.get_queryset())
        obj = queryset.get(user=self.request.user)
        if obj:
            self.check_object_permissions(self.request, obj)
            return obj
        else:
            raise NotFound("No user data found!")

    def perform_update(self, serializer):
        instance = serializer.save(user=self.request.user)
        return instance


class ProviderPopCreateAPIView(generics.CreateAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = ProviderPop.objects.all()
    serializer_class = ProviderPopSerializer
    def perform_create(self, serializer):
        try:
            provider = Provider.objects.get(user=self.request.user)
        except:
            provider = Provider.objects.create(user=self.request.user)
        serializer.save(provider=provider)


class ProviderPopRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    authentication_classes = (TokenAuthentication,)
    queryset = ProviderPop.objects.all()
    lookup_url_kwarg = "id"

    def get_serializer_class(self):
        if self.request.method == "GET":
            return ProviderPopListSerializer
        return ProviderPopSerializer

    def get_queryset(self):
        pops = ProviderPop.objects.filter(active=True)
        return self.get_serializer_class().with_eager_load(pops)

    def perform_update(self, serializer):
        try:
            provider = Provider.objects.get(user=self.request.user)
        except:
            raise NotFound("Provider data not found for user! Contact Admin.")

        instance = serializer.save(provider=provider)
        return instance

    def perform_delete(self, request, *args, **kwargs):
        try:
            pop = ProviderPop.objects.get(kwargs['id'])
        except:
            raise NotFound("Pop instance not found!")

        if not pop.provider.user == request.user:
            raise PermissionDenied("You can only delete your own pops!")

        pop.delete()



class ProviderPopListAPIView(APIView):
    authentication_classes = (TokenAuthentication,)
    def get(self, request):
        f_list = []
        # Append filter args to filterarg list
        if 'ownpops' not in request.query_params:
            # in case of global payload the endpoint might use provider/isp_type filter
            if 'provider' in request.query_params:
                provider_id = request.query_params['provider']
                if provider_id != '':
                    f_list.append(Q(provider__user__id=provider_id))
            if 'isp_type' in request.query_params:
                type_id = request.query_params['isp_type']
                if type_id != '':
                    f_list.append(Q(provider__isp_type__id=type_id))
        else:
            f_list.append(Q(provider__user=request.user))

        if 'ward' in request.query_params:
            ward_id = request.query_params['ward']
            if ward_id != '':
                f_list.append(Q(ward__id=ward_id))

        if 'upazilla' in request.query_params:
            upz_id = request.query_params['upazilla']
            if upz_id != '':
                f_list.append(Q(upazilla__id=upz_id))

        if 'district' in request.query_params:
            dist_id = request.query_params['district']
            if dist_id != '':
                f_list.append(Q(district__id=dist_id))

        if 'division' in request.query_params:
            div_id = request.query_params['division']
            if div_id != '':
                f_list.append(Q(division__id=div_id))

        # execute filtered query
        pops = ProviderPop.objects.filter(active=True)
        if len(f_list) > 0:
            pops = pops.filter(reduce(operator.and_, f_list))

        # string search
        if 'q' in request.query_params:
            q_str = request.query_params['q']
            q_str = q_str.strip()
            if q_str != '':
                q_list = [
                    Q(contact_info__icontains=q_str),
                    Q(provider__name__icontains=q_str),
                    Q(provider__isp_type__name__icontains=q_str),
                ]
                pops = pops.filter(reduce(operator.or_, q_list))

        # Geometrical sorting
        if 'lat' in request.query_params:
            if 'lon' in request.query_params:
                lat = request.query_params['lat']
                lon = request.query_params['lon']
                try:
                    lat = float(lat)
                    lon = float(lon)
                    user_location = Point(lon, lat, srid=4326)

                    radius_km = get_config().map_filter_radius if get_config() is not None else 5
                    if 'radius' in request.query_params:
                        try:
                            radius_km = float(request.query_params['radius'])
                            if radius_km < 0:
                                radius_km = radius_km * (-1)
                        except:
                            pass

                    radius = radius_km * 1000

                    pops = pops.filter(
                        loc__distance_lt=(
                            user_location,
                            Distance(m=radius)
                        )
                    ).order_by(GeometryDistance("loc", user_location))
                except:
                    pass

        total_count = pops.count()
        limit = None
        offset = 0
        if 'limit' in request.query_params:
            try:
                limit = int(request.query_params['limit'])
            except:
                pass

        if 'offset' in request.query_params:
            try:
                offset = int(request.query_params['offset'])
            except:
                pass
        # pagination
        pops = pops[offset:(limit + offset) if limit is not None else None]
        page_count = pops.count()

        # serialize content
        pops = ProviderPopListSerializer.with_eager_load(pops)
        pops_data = ProviderPopListSerializer(pops, many=True).data

        return Response({
            "count": {
                "total": total_count,
                "page": page_count,
            },
            "pagination" : {
                "limit": limit,
                "offset": offset,
            },
            "data": pops_data
        })