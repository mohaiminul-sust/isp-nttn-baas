from django.contrib import admin
# from django.contrib.gis.admin import GeoModelAdmin, OSMGeoAdmin
from leaflet.admin import LeafletGeoAdmin
from django.conf import settings
from django_admin_relation_links import AdminChangeLinksMixin
from import_export.admin import ImportExportMixin, ExportActionMixin

# Register your models here.
from .models import Provider, ProviderPop, Division, District, Upazilla, Ward, IspType, PlatformConfig
from .forms import ProviderPopForm
from .resources import ProviderResource, ProviderPopResource


class PlatformConfigAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('config_name', 'active', 'map_filter_radius','created_date', 'updated_date',)

    def has_delete_permission(self, request, obj=None):
        if PlatformConfig.objects.all().count() > 1:
            return True
        return False

admin.site.register(PlatformConfig, PlatformConfigAdmin)


class DivisionAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('id', 'name', 'created_date', 'updated_date',)
    search_fields = ('name',)
    ordering = ('id',)

admin.site.register(Division, DivisionAdmin)


class DistrictAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('id', 'name', 'division', 'created_date', 'updated_date',)
    list_filter = ('division',)
    raw_id_fields = ('division',)
    search_fields = ('name',)
    ordering = ('id',)
    autocomplete_lookup_fields = {
        'fk': ['division'],
    }

admin.site.register(District, DistrictAdmin)


class UpazillaAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('id', 'name', 'district', 'division', 'created_date', 'updated_date',)
    list_filter = ('district__division', 'district')
    search_fields = ('name',)
    ordering = ('id',)
    raw_id_fields = ('district',)
    autocomplete_lookup_fields = {
        'fk': ['district'],
    }

admin.site.register(Upazilla, UpazillaAdmin)


class WardAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('id', 'name', 'upazilla', 'district', 'division', 'created_date', 'updated_date',)
    list_filter = ('upazilla__district__division', 'upazilla__district', 'upazilla')
    search_fields = ('name',)
    ordering = ('id',)
    raw_id_fields = ('upazilla',)
    autocomplete_lookup_fields = {
        'fk': ['upazilla'],
    }

admin.site.register(Ward, WardAdmin)


class ProviderInline(admin.TabularInline):
    extra = 0
    model = Provider
    show_change_link = True
    fields = ('name', 'user', 'active', 'provider_type', 'lat','lon')

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False


class IspTypeAdmin(admin.ModelAdmin):
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    inlines = (ProviderInline,)
    list_display = ('id', 'name', 'created_date', 'updated_date',)
    ordering = ('id',)

admin.site.register(IspType, IspTypeAdmin)


class ProviderPopInline(admin.TabularInline):
    extra = 0
    model = ProviderPop
    show_change_link = True
    fields = ('id', 'active', 'pop_address', 'division', 'district', 'upazilla','ward', 'lat', 'lon',)

    def has_add_permission(self, request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def isp_type(self, obj):
        return obj.provider.isp_type


class ProviderAdmin(AdminChangeLinksMixin, ImportExportMixin, ExportActionMixin, LeafletGeoAdmin):
    resource_class = ProviderResource
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    inlines = (ProviderPopInline,)
    list_display = ('name', 'provider_type', 'isp_type', 'user', 'active', 'total_pops', 'pops_link', 'division', 'district', 'created_date', 'updated_date')
    changelist_links = [
        ('pops', {
            'label': 'Show POPs',
            'model': 'ProviderPop'
        })
    ]
    list_select_related = ('user',)
    list_filter = ('active', 'provider_type', 'isp_type')
    search_fields = ('name', 'user__email')
    ordering = ('name',)
    raw_id_fields = ('isp_type', 'division', 'district')
    autocomplete_lookup_fields = {
        'fk': ['isp_type', 'division', 'district'],
    }

admin.site.register(Provider, ProviderAdmin)


class ProviderPopAdmin(AdminChangeLinksMixin, ImportExportMixin, ExportActionMixin, LeafletGeoAdmin):
    resource_class = ProviderPopResource
    form = ProviderPopForm
    date_hierarchy = 'created_date'
    list_per_page = settings.ADMIN_LIST_PAGE_ITEMS
    list_display = ('id', 'provider_link', 'isp_type', 'active', 'pop_type', 'division', 'district', 'upazilla', 'ward', 'lat', 'lon', 'contact_info', 'equipments', 'created_date', 'updated_date')
    change_links = ['provider']
    list_select_related= ('provider', 'division', 'district', 'upazilla', 'ward',)
    list_filter = ('provider__isp_type', 'provider', 'active', 'division', 'district', 'upazilla', 'ward')
    search_fields = ('provider__name', 'provider__user__email')
    ordering = ('id',)
    raw_id_fields = ('provider', 'division', 'district', 'upazilla', 'ward')
    autocomplete_lookup_fields = {
        'fk': ['provider', 'division', 'district', 'upazilla', 'ward'],
    }

    def isp_type(self, obj):
        return obj.provider.isp_type


admin.site.register(ProviderPop, ProviderPopAdmin)