from django.conf import settings
from django.dispatch import receiver
from django.db.models.signals import post_save, pre_save, m2m_changed
from django.contrib.gis.geos import Point

from .models import ProviderPop, Provider, PlatformConfig


@receiver(pre_save, sender=Provider)
def update_loc_for_provider(sender, instance=None, created=False, **kwargs):
    if instance.lat is not None and instance.lon is not None:
        instance.loc = Point(instance.lon, instance.lat, srid=4326)
    else:
        instance.loc = None


@receiver(pre_save, sender=ProviderPop)
def update_loc_for_pop(sender, instance=None, created=False, **kwargs):
    if instance.lat is not None and instance.lon is not None:
        instance.loc = Point(instance.lon, instance.lat, srid=4326)
    else:
        instance.loc = None


@receiver(post_save, sender=PlatformConfig)
def maintain_active_instance_singularity(sender, instance=None, created=False, **kwargs):
    other_instances = sender.objects.exclude(pk=instance.id)
    if instance.active:
        for obj in other_instances:
            obj.active = False
            obj.save()
    else:
        if not other_instances.exists():
            instance.active = True
            instance.save()