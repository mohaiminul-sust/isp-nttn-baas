from import_export import resources
from import_export.fields import Field

from .models import Provider, ProviderPop


class ProviderResource(resources.ModelResource):
    total_pops = Field()
    class Meta:
        model = Provider
        # skip_unchanged = True
        # report_skipped = True
        import_id_fields = ('user',)

    def dehydrate_total_pops(self, obj):
        return obj.total_pops()


class ProviderPopResource(resources.ModelResource):
    class Meta:
        model = ProviderPop
        import_id_fields = ('id',)