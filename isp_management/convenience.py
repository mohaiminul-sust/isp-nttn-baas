import json, random
from faker import Faker
from django.contrib.gis.geos import Point

from django.conf import settings
from .models import Division, District, Upazilla, Ward, Provider, ProviderPop, IspType, Provider, PlatformConfig
from users.models import User


def get_config():
    all_configs = PlatformConfig.objects.all()
    active_configs = all_configs.filter(active=True)

    if all_configs.exists():
        if active_configs.exists():
            return active_configs.first()
        else:
            return all_configs.first()
    else:
        return None


def load_json_from(path):
    full_path = settings.BASE_DIR + '/json/' + path
    with open(full_path) as json_file:
        data = json.load(json_file)
        return data


def populate_isp_types():
    data = load_json_from('isp_types.json')
    isp_types = [IspType(id=num, name=entry) for num, entry in enumerate(data, start=1)]
    IspType.objects.bulk_create(isp_types)
    return "Created {} entries.".format(IspType.objects.count())


def populate_providers():
    data = load_json_from('isp_names.json')
    providers=[]
    for entry in data:
        provider = Provider()
        f_name = "".join(entry["name"].split())
        provider.user = User.objects.create(username=f_name, email=f_name+"@ispnttn.com")
        provider.name = entry["name"]
        provider.isp_type = IspType.objects.filter(name=entry["type"]).first() if IspType.objects.filter(name=entry["type"]).exists() else None
        providers.append(provider)

    Provider.objects.bulk_create(providers)
    return "Created {} entries.".format(len(providers))


def populate_divisions():
    data = load_json_from('divisions.json')
    divisions = [Division(id=entry['id'], name=entry['name']) for entry in data['data']]
    Division.objects.bulk_create(divisions)


def populate_districts():
    data = load_json_from('districts.json')
    districts = [District(id=entry['id'], name=entry['name'], division=Division.objects.get(pk=entry['division_id'])) for entry in data['data']]
    District.objects.bulk_create(districts)


def populate_upazillas():
    data = load_json_from('upazillas.json')
    upazillas = [Upazilla(id=entry['id'], name=entry['name'], district=District.objects.get(pk=entry['district_id'])) for entry in data['data']]
    Upazilla.objects.bulk_create(upazillas)


def populate_wards():
    wards = []
    # for upazilla in Upazilla.objects.all():
    #     rand_range = random.randint(5, 11)
    #     wards_of_up = [ Ward(name=str(random.randint(30, 300)), upazilla=upazilla) for i in range(rand_range) ]
    #     wards.extend(wards_of_up)
    data = load_json_from('wards.json')
    wards = [ Ward(name = entry["name"], upazilla = Upazilla.objects.get(id=entry["thana_id"]) if entry["thana_id"] is not None else None) for entry in data ]
    Ward.objects.bulk_create(wards)
    return "Created {} entries".format(Ward.objects.count())


def populate_pops():
    data = load_json_from('pop_data.json')
    p_pops = []
    for num, entry in enumerate(data, start=1):
        pop = ProviderPop()
        pop.provider = Provider.objects.filter(name=str(entry["name"]).strip()).first() if Provider.objects.filter(name=str(entry["name"]).strip()).exists() else None
        pop.pop_address = str(entry["address"]).strip()
        pop.division = Division.objects.get(id=entry["division_id"]) if entry["division_id"] is not None else None
        pop.district = District.objects.get(id=entry["district_id"]) if entry["district_id"] is not None else None
        pop.upazilla = Upazilla.objects.get(id=entry["thana_id"]) if entry["thana_id"] is not None else None
        pop.ward = Ward.objects.filter(name=str(entry["ward"]).strip()).first() if Ward.objects.filter(name=str(entry["ward"]).strip()).exists() else None
        pop.lat = float(entry["latitude"])
        pop.lon = float(entry["longitude"])
        pop.loc = Point(float(entry["longitude"]), float(entry["latitude"]), srid=4326)
        pop.contact_info = str(entry["contact"]).strip() if entry["contact"] is not None else ""
        pop.equipments = str(entry["equipment"]).strip() if entry["equipment"] is not None else ""
        p_pops.append(pop)
        print("Serialized pop from raw json : {}".format(num))

    print("Start bulk record creation..")
    ProviderPop.objects.bulk_create(p_pops)
    return "Created {} entries from {} records".format(ProviderPop.objects.count(), len(data))


def get_random_provider():
    provider_id_list = list(Provider.objects.all().values_list('user', flat=True))
    sel_id = random.sample(provider_id_list, len(provider_id_list))[0]
    return Provider.objects.get(pk=sel_id)


def get_random_division():
    div_id_list = list(Division.objects.all().values_list('id', flat=True))
    sel_id = random.sample(div_id_list, len(div_id_list))[0]
    return Division.objects.get(pk=sel_id)


def get_random_district():
    dist_id_list = list(District.objects.all().values_list('id', flat=True))
    sel_id = random.sample(dist_id_list, len(dist_id_list))[0]
    return District.objects.get(pk=sel_id)


def get_random_upazilla():
    upz_id_list = list(Upazilla.objects.all().values_list('id', flat=True))
    sel_id = random.sample(upz_id_list, len(upz_id_list))[0]
    return Upazilla.objects.get(pk=sel_id)


def get_random_ward():
    ward_id_list = list(Ward.objects.all().values_list('id', flat=True))
    sel_id = random.sample(ward_id_list, len(ward_id_list))[0]
    return Ward.objects.get(pk=sel_id)


def populate_dummy_pops(count=3000):
    fake_gen = Faker()
    pops = []
    for i in range(count):
        ward = get_random_ward()
        upazilla = ward.upazilla
        division = ward.division()
        district = ward.district()
        lat, lon = fake_gen.latlng()
        loc = Point(float(lon), float(lat), srid=4326)
        pop = ProviderPop(provider=get_random_provider(), pop_address=fake_gen.address(), division=division, district=district, upazilla=upazilla, ward=ward, contact_info="{}\nContact Number - {}".format(fake_gen.name(), fake_gen.phone_number()), lat=lat, lon=lon, loc=loc, equipments=fake_gen.paragraph())
        pops.append(pop)


    ProviderPop.objects.bulk_create(pops)
    return "Generated : {}, Uploaded : {}".format(len(pops), ProviderPop.objects.all().count())